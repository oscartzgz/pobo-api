class ChangeCodeToStringAtSaleNote < ActiveRecord::Migration[6.1]
  def change
    change_column :sale_notes, :code, :string
  end
end
