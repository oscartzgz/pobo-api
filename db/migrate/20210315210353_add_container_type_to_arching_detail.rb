class AddContainerTypeToArchingDetail < ActiveRecord::Migration[6.1]
  def change
    add_column :arching_details, :container_type, :string
  end
end
