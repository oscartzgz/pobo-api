class CreateCagesDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :cages_details do |t|
      t.references :live_chicken_reception, null: false, foreign_key: true
      t.integer :chickens_number
      t.integer :cages_number
      t.float :cage_weight
      t.float :total_weight

      t.timestamps
    end
  end
end
