class AddCodeToSalePayment < ActiveRecord::Migration[6.1]
  def change
    add_column :sale_payments, :code, :string
  end
end
