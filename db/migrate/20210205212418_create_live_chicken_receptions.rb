class CreateLiveChickenReceptions < ActiveRecord::Migration[6.1]
  def change
    create_table :live_chicken_receptions do |t|
      t.references :provider, null: false, foreign_key: true
      t.integer :code
      t.date :date
      t.string :responsible_name
      t.text :details
      t.decimal :kg_price, precision: 12, scale: 2, default: 0.0, null: false
      t.decimal :amount, precision: 12, scale: 2, default: 0.0, null: false

      t.timestamps
    end
  end
end
