class CreateSalePayments < ActiveRecord::Migration[6.1]
  def change
    create_table :sale_payments do |t|
      t.references :client, null: false, foreign_key: true
      t.date :date
      t.decimal :import, precision: 12, scale: 2, default: 0.0, null: false

      t.timestamps
    end
  end
end
