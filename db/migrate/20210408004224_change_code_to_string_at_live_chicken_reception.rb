class ChangeCodeToStringAtLiveChickenReception < ActiveRecord::Migration[6.1]
  def change
    change_column :live_chicken_receptions, :code, :string
  end
end
