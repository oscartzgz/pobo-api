class CreateExpenses < ActiveRecord::Migration[6.1]
  def change
    create_table :expenses do |t|
      t.string :kind, null: false
      t.date :date, null: false
      t.decimal :import, precision: 12, scale: 2, default: 0.0, null: false
      t.text :details

      t.timestamps
    end
  end
end
