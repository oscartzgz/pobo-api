class CreateSalePaymentDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :sale_payment_details do |t|
      t.references :sale_note, null: false, foreign_key: true
      t.references :sale_payment, null: false, foreign_key: true
      t.boolean :complete_payment, null: false, default: true

      t.timestamps
    end
  end
end
