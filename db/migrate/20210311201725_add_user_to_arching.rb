class AddUserToArching < ActiveRecord::Migration[6.1]
  def change
    add_reference :archings, :user, null: false, foreign_key: true
  end
end
