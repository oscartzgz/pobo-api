class CreateProviderPayments < ActiveRecord::Migration[6.1]
  def change
    create_table :provider_payments do |t|
      t.references :provider, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.date :date, null: false
      t.decimal :import, precision: 12, scale: 2, default: 0.0, null: false
      t.text :details

      t.timestamps
    end
  end
end
