class CreateArchingDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :arching_details do |t|
      t.references :arching, null: false, foreign_key: true
      t.string  :product_type
      t.integer :racks_number
      t.integer :chickens_number

      t.timestamps
    end
  end
end
