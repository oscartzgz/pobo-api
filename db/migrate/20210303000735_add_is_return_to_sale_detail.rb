class AddIsReturnToSaleDetail < ActiveRecord::Migration[6.1]
  def change
    add_column :sale_details, :is_return, :boolean, default: false, null: false
  end
end
