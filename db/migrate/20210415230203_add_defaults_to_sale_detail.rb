class AddDefaultsToSaleDetail < ActiveRecord::Migration[6.1]
  def change
    change_column :sale_details, :quantity, :integer, default: 1
    change_column :sale_details, :raw_weight, :float, default: 0
    change_column :sale_details, :sale_price, :float, default: 0
  end
end
