class AddRakeWeightToProvider < ActiveRecord::Migration[6.1]
  def change
    add_column :providers, :rake_weight, :float, null: false, default: 0.0
  end
end
