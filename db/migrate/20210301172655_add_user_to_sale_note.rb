class AddUserToSaleNote < ActiveRecord::Migration[6.1]
  def change
    add_reference :sale_notes, :user, null: false, foreign_key: true
  end
end
