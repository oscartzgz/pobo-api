class ChangeCodeToStringAtArching < ActiveRecord::Migration[6.1]
  def change
    change_column :archings, :code, :string
  end
end
