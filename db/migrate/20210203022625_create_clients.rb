class CreateClients < ActiveRecord::Migration[6.1]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :department
      t.string :municipality
      t.string :address
      t.string :email
      t.string :phone
      t.text :details

      t.timestamps
    end
  end
end
