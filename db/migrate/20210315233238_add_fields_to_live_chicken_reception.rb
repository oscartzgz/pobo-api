class AddFieldsToLiveChickenReception < ActiveRecord::Migration[6.1]
  def change
    add_column :live_chicken_receptions, :chicken_return, :integer
    add_column :live_chicken_receptions, :weight_chicken_return, :float
  end
end
