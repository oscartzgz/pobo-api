class CreateSaleNotes < ActiveRecord::Migration[6.1]
  def change
    create_table :sale_notes do |t|
      t.references :client, null: false, foreign_key: true
      t.date :buy_reference_date, null: false
      t.date :date, null: false
      t.integer :code

      t.timestamps
    end
  end
end
