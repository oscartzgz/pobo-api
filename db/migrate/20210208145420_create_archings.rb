class CreateArchings < ActiveRecord::Migration[6.1]
  def change
    create_table :archings do |t|
      t.date :date, null: false
      t.integer :code
      t.decimal :price_per_chicken, precision: 12, scale: 2, default: 0.0
      t.integer :chicken_return
      t.float :weight_chicken_return
      t.decimal :amount, precision: 12, scale: 2, default: 0.0, null: false

      t.timestamps
    end
  end
end
