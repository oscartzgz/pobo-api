class AddUserToLiveChickenReception < ActiveRecord::Migration[6.1]
  def change
    add_reference :live_chicken_receptions, :user, null: false, foreign_key: true
  end
end
