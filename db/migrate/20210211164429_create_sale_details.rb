class CreateSaleDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :sale_details do |t|
      t.references :sale_note, null: false, foreign_key: true
      t.string :container_type
      t.integer :quantity
      t.float :raw_weight
      t.string :content
      t.float :sale_price

      t.timestamps
    end
  end
end
