class AddFieldsToProvider < ActiveRecord::Migration[6.1]
  def change
    add_column :providers, :our_debt_date, :date, null: false, default: '2020-11-30'
    add_column :providers, :our_debt, :decimal, precision: 12, scale: 2, default: 0.0, null: false
  end
end
