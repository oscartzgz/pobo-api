# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_09_17_152223) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "arching_details", force: :cascade do |t|
    t.bigint "arching_id", null: false
    t.string "product_type"
    t.integer "racks_number"
    t.integer "chickens_number"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "container_type"
    t.index ["arching_id"], name: "index_arching_details_on_arching_id"
  end

  create_table "archings", force: :cascade do |t|
    t.date "date", null: false
    t.string "code"
    t.decimal "price_per_chicken", precision: 12, scale: 2, default: "0.0"
    t.integer "chicken_return"
    t.float "weight_chicken_return"
    t.decimal "amount", precision: 12, scale: 2, default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["user_id"], name: "index_archings_on_user_id"
  end

  create_table "cages_details", force: :cascade do |t|
    t.bigint "live_chicken_reception_id", null: false
    t.integer "chickens_number"
    t.integer "cages_number"
    t.float "cage_weight"
    t.float "total_weight"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["live_chicken_reception_id"], name: "index_cages_details_on_live_chicken_reception_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name"
    t.string "department"
    t.string "municipality"
    t.string "address"
    t.string "email"
    t.string "phone"
    t.text "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "expenses", force: :cascade do |t|
    t.string "kind", null: false
    t.date "date", null: false
    t.decimal "import", precision: 12, scale: 2, default: "0.0", null: false
    t.text "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "live_chicken_receptions", force: :cascade do |t|
    t.bigint "provider_id", null: false
    t.string "code"
    t.date "date"
    t.string "responsible_name"
    t.text "details"
    t.decimal "kg_price", precision: 12, scale: 2, default: "0.0", null: false
    t.decimal "amount", precision: 12, scale: 2, default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.integer "chicken_return"
    t.float "weight_chicken_return"
    t.index ["provider_id"], name: "index_live_chicken_receptions_on_provider_id"
    t.index ["user_id"], name: "index_live_chicken_receptions_on_user_id"
  end

  create_table "provider_payments", force: :cascade do |t|
    t.bigint "provider_id", null: false
    t.bigint "user_id", null: false
    t.date "date", null: false
    t.decimal "import", precision: 12, scale: 2, default: "0.0", null: false
    t.text "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["provider_id"], name: "index_provider_payments_on_provider_id"
    t.index ["user_id"], name: "index_provider_payments_on_user_id"
  end

  create_table "providers", force: :cascade do |t|
    t.string "name"
    t.string "department"
    t.string "municipality"
    t.string "address"
    t.string "email"
    t.string "phone"
    t.string "details"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "rake_weight", default: 0.0, null: false
    t.date "our_debt_date", default: "2020-11-30", null: false
    t.decimal "our_debt", precision: 12, scale: 2, default: "0.0", null: false
  end

  create_table "sale_details", force: :cascade do |t|
    t.bigint "sale_note_id", null: false
    t.string "container_type"
    t.integer "quantity", default: 1
    t.float "raw_weight", default: 0.0
    t.string "content"
    t.float "sale_price", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "is_return", default: false, null: false
    t.index ["sale_note_id"], name: "index_sale_details_on_sale_note_id"
  end

  create_table "sale_notes", force: :cascade do |t|
    t.bigint "client_id", null: false
    t.date "buy_reference_date", null: false
    t.date "date", null: false
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "user_id", null: false
    t.index ["client_id"], name: "index_sale_notes_on_client_id"
    t.index ["user_id"], name: "index_sale_notes_on_user_id"
  end

  create_table "sale_payment_details", force: :cascade do |t|
    t.bigint "sale_note_id", null: false
    t.bigint "sale_payment_id", null: false
    t.boolean "complete_payment", default: true, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sale_note_id"], name: "index_sale_payment_details_on_sale_note_id"
    t.index ["sale_payment_id"], name: "index_sale_payment_details_on_sale_payment_id"
  end

  create_table "sale_payments", force: :cascade do |t|
    t.bigint "client_id", null: false
    t.date "date"
    t.decimal "import", precision: 12, scale: 2, default: "0.0", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "code"
    t.index ["client_id"], name: "index_sale_payments_on_client_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest", null: false
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "arching_details", "archings"
  add_foreign_key "archings", "users"
  add_foreign_key "cages_details", "live_chicken_receptions"
  add_foreign_key "live_chicken_receptions", "providers"
  add_foreign_key "live_chicken_receptions", "users"
  add_foreign_key "provider_payments", "providers"
  add_foreign_key "provider_payments", "users"
  add_foreign_key "sale_details", "sale_notes"
  add_foreign_key "sale_notes", "clients"
  add_foreign_key "sale_notes", "users"
  add_foreign_key "sale_payment_details", "sale_notes"
  add_foreign_key "sale_payment_details", "sale_payments"
  add_foreign_key "sale_payments", "clients"
end
