# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.delete_all
Client.delete_all
Provider.delete_all
LiveChickenReception.delete_all
CagesDetail.delete_all
ArchingDetail.delete_all
Arching.delete_all

users = [
  {email: 'v@email.com', password: '123456', name: 'VH'},
  {email: 'j@email.com', password: '123456', name: 'JMR'},
  {email: 'r@email.com', password: '123456', name: 'RUM'}
]

clients = [
  {name: 'Poly Chui', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Andres Chui', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Eugenio C.', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Ruth Cespedes', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Franklin Choque', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Poly Chui', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Juan Orellana', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Raul Calisaya', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Jovana Ramos', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Julia Q.', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Ely Leuca', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Mary Sirpa', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Ceferina Ticona', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Ruth Alanoca', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Betthy Chuquimia', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Blanca Rocha', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Hilda Lima', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Virginia Sirpa', department: 'La Paz', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
]

providers = [
  {name: 'Arancibia', department: 'Cochabamba', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
  {name: 'Pio Lindo', department: 'Cochabamba', municipality: '', address: Faker::Address.full_address, email: Faker::Internet.email, phone: Faker::PhoneNumber.cell_phone },
]

live_cage_receptions = 100.times.map do |index|
  {
    provider: nil,
    code: index,
    date: Faker::Date.between(from: '2021-01-01', to: Date.today),
    responsible_name: Faker::Name.name_with_middle,
    details: Faker::Lorem.paragraph,
    chicken_return: rand(10),
    amount: (rand(1100) + 1000),

    cages_details_attributes: rand(30).times.map do |i|
      {
        # live_chicken_reception_id:
        chickens_number: 30,
        cages_number: 5, 
        cage_weight: 6.8,
        total_weight: 120 + rand(40),
    }
    end
  }
end

archings = 40.times.map do |index|
  {
    code: rand(99999),
    date: Faker::Date.between(from: '2021-01-01', to: Date.today),
    chicken_return: rand(10),
    weight_chicken_return: rand(10) + rand.round(1),
    amount: (rand(30) + 30),

    arching_details_attributes: rand(10).times.map do |i|
      {
        racks_number: rand(11),
        chickens_number: (rand(10) + 20), 
      }
    end
  }
end


sale_notes = 100.times.map do |index|
  date = Faker::Date.between(from: '2021-01-01', to: Date.today)
  {
    client: nil,
    buy_reference_date: date,
    date: (date + 1.day),
    code: rand(99999),
    sale_details_attributes: rand(5).times.map do |i|
      {
        container_type: ['Rejilla', 'Bolsa'].sample,
        quantity: (rand(10) + 20),
        raw_weight: rand(300),
        content: ['Pollo', 'Alas', 'Higado'].sample,
        sale_price: ( rand(1000) + 1000 )
      }
    end
  }
end


users.each do |user_data|
  User.create(user_data)
end
puts "Users was populated from seed file"

clients.each do |client_data|
  Client.create(client_data)
end
puts "Clients was populated from seed file"


providers.each do |provider_data|
  Provider.create(provider_data)
end
puts "Providers was populated from seed file"

# live_cage_receptions.each do |lcr_data|
#   lcr_data[:provider_id] = Provider.order(Arel.sql('RANDOM()')).first.id
#   lcr_data[:user_id] = User.order(Arel.sql('RANDOM()')).first.id
#   lcr = LiveChickenReception.create(lcr_data)
# end
# puts "LiveChickenReceptions was populated from seed file"

# archings.each do |arching_data|
#   Arching.create(arching_data)
# end
# puts "Archings was populated from seed file"

# clients = Client.all
# sale_notes.each do |sale_note_data|
#   sale_note_data[:client] = clients.sample
#   sn = SaleNote.create(sale_note_data)
# end
# puts "SaleNotes was populated from seed file "


# VH Martes -> Miercoles
# 
#
#

# Arqueo Faineado Agregar Por Item Tipo de ?? (Presa, Pollo)  