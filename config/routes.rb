Rails.application.routes.draw do
  resources :users,     only: %i[index show create update destroy]

  post '/tokens', to: 'tokens#create'
  get '/tokens', to: 'tokens#show'

  
  resources :clients,   only: %i[index show create update destroy] do
    get 'sale_notes', to: 'clients#sale_notes'
    get 'sale_notes_with_complete_payment', to: 'clients#sale_notes_with_complete_payment'
    get 'sale_notes_without_complete_payment', to: 'clients#sale_notes_without_complete_payment'
  end

  resources :providers, only: %i[index show create update destroy] do 
    resources :provider_payments, only: %i[index show create update destroy]
  end

  resources :live_chicken_receptions do
    resources :cages_details, only: %i[index update destroy]
  end

  resources :archings do 
    resources :arching_details, only: %i[index]
  end

  resources :sale_notes do
    member do 
      get 'pdf', to: 'pdfs#sale_note'
    end
    resources :sale_details, only: %i[ index ]
  end

  resources :sale_payments, only: %i[index show create update destroy] do
    resources :sale_payment_details, only: %i[index]
  end

  resources :expenses, only: %i[ index create update destroy]

  get '/calendar', to: 'calendar#index'
  get '/calendar/date', to: 'calendar#date'

  get '/reports/reception_and_arching', to: 'reports#reception_and_arching'
  get '/reports/provider_resume',       to: 'reports#provider_resume'
  get '/reports/collections',           to: 'reports#collections'

  get '/resume', to: 'resume#index'
end
