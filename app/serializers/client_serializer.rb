class ClientSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :department, :municipality, :address, :email, :phone, :details
end
