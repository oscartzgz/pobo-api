class LiveChickenReceptionSerializer
  include FastJsonapi::ObjectSerializer
  attributes :user_id, :provider_id, :date, :code, :responsible_name, :details, :kg_price,
    :raw_weight,
    :cages_weight,
    :total_chickens,
    :slaughter_import,
    :chicken_weight,
    :decrease,
    :net_weight,
    :final_net_weight,
    :weight_avg,
    :chicken_return,
    :weight_chicken_return,
    :import,
    :final_import

  attribute :provider_name do |object|
    object.provider.name
  end

  attribute :user_name do |object|
    object.user.name
  end

  # attribute :date do |object|
  #   object.date.strftime('%d/%m/%Y')
  # end

  attribute :date_short do |object|
    object.date.strftime('%d/%m/%Y')
  end

  attribute :code_formated do |object|
    object.code
  end

  belongs_to :provider
  has_many   :cages_details
end
