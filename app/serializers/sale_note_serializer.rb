class SaleNoteSerializer
  include FastJsonapi::ObjectSerializer
  attributes :user_id, :client_id, :buy_reference_date, :date, :code,
    :raw_weight,
    :net_weight,
    :chicken_raw_weight,
    :chicken_weight,
    :returned_chicken_weight,
    :chicken_untare,
    :chicken_net_weight,
    :chicken_amount,
    :returned_chicken_amount,
    :total_amount

  attribute :days_ago do |object|
    (Date.today - object.date).to_i
  end

  attribute :user_name do |object|
    object.user.name
  end

  attribute :code_formated do |object|
    object.code
  end

  attribute :date_formated do |object|
    object.date.strftime('%d/%m/%Y')
  end

  attribute :buy_reference_date_short do |obj|
    obj.buy_reference_date.strftime('%d/%b/%y')
  end
  attribute :client_name do |obj|
    obj.client.name
  end
  belongs_to :client
  has_many :sale_details
end
