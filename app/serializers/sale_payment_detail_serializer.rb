class SalePaymentDetailSerializer
  include FastJsonapi::ObjectSerializer
  attributes :complete_payment


  attribute :code do |object|
    object.sale_note.code
  end

  attribute :total_amount do |object|
    object.sale_note.total_amount
  end

  attribute :date do |object|
    object.sale_note.date
  end

  attribute :date_formated do |object|
    object.sale_note.date.strftime("%d/%m/%Y")
  end
end
