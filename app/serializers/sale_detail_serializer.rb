class SaleDetailSerializer
  include FastJsonapi::ObjectSerializer
  attributes :is_return, :container_type, :quantity, :raw_weight, :content, :sale_price,
    :rack_untare,
    :chicken_net_weight,
    :net_weight,
    :import
end
