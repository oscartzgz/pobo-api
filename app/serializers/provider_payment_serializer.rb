class ProviderPaymentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :user_id, :provider_id, :date, :import, :details

  attribute :user_name do |object|
    object.user.name
  end

  attribute :date_short do |object|
    object.date.strftime('%d/%m/%y')
  end

  attribute :provider_name do |object|
    object.provider.name
  end
end
