class ArchingDetailSerializer
  include FastJsonapi::ObjectSerializer
  attributes :product_type, :racks_number, :chickens_number, :container_type
end
