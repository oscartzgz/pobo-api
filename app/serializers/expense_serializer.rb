class ExpenseSerializer
  include FastJsonapi::ObjectSerializer
  attributes :kind, :date, :import, :details

  attribute :date_short do |object|
    object.date.strftime('%d/%m/%y')
  end
end
