class ProviderSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :department, :municipality, :address, :email, :phone, :details, :rake_weight, :our_debt_date, :our_debt

  attribute :actual_doubt do |object|
    object.actual_doubt
  end
end
