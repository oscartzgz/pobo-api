class CagesDetailSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :chickens_number, :cages_number, :cage_weight, :total_weight
end
