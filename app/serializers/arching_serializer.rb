class ArchingSerializer
  include FastJsonapi::ObjectSerializer
  attributes :code, :chicken_return, :weight_chicken_return, :amount, :price_per_chicken,
    :total_chickens,
    :user_id

  attribute :date do |object|
    object.date.strftime('%d/%b/%y')
  end

  attribute :code_formated do |object|
    object.code
  end

  attribute :user_name do |object|
    object.user.name
  end
end
