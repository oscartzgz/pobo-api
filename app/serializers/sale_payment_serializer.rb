class SalePaymentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :client_id, :date, :import, :sale_notes_amount, :difference, :code

  attribute :date_formated do  |object|
    object.date.strftime('%d/%m/%Y')
  end

  attribute :client_name do  |object|
    object.client.name
  end
end
