class SalePaymentsController < ApplicationController
  before_action :check_login
  before_action :set_sale_payment, only: %i(show edit update destroy sale_payment_details)

  def index
    @sale_payments = SalePayment.all.order(date: :desc)
    render json: SalePaymentSerializer.new(@sale_payments).serializable_hash, status: :ok
  end

  def show
    render json: SalePaymentSerializer.new(@sale_payment).serializable_hash, status: :ok
  end

  def create
    @sale_payment = SalePayment.new(sale_payment_params)
    
    if @sale_payment.save
      render json: SalePaymentSerializer.new(@sale_payment).serializable_hash, status: :created
    else
      render json: { errors: @sale_payment.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @sale_payment.update(sale_payment_params)
      render json: SalePaymentSerializer.new(@sale_payment).serializable_hash, status: :ok
    else
      render json: { errors: @sale_payment.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @sale_payment.destroy
    head :no_content
  end

  private
  def sale_payment_params
    params.require(:sale_payment).permit(:client_id, :date, :import, :code,
                                      sale_payment_details_attributes: [:id, :sale_note_id, :sale_payment_id, :complete_payment, :_destroy])
  end

  def set_sale_payment
    @sale_payment = SalePayment.find(params[:id])
  end
end
