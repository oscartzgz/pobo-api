class SalePaymentDetailsController < ApplicationController
  before_action :check_login
  before_action :set_sale_payment

  def index
    sale_payment_details = @sale_payment.sale_payment_details

    render json: SalePaymentDetailSerializer.new(sale_payment_details).serializable_hash, status: :ok
  end

  private
  def set_sale_payment
    @sale_payment = SalePayment.find(params[:sale_payment_id])
  end
end
