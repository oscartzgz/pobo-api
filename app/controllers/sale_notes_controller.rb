class SaleNotesController < ApplicationController
  include Paginable
  # before_action :check_login, except: [:show]
  before_action :check_login
  before_action :set_sale_note, only: %i(show edit update destroy)

  def index
    @sale_notes = SaleNote.page(current_page)
                          .per(per_page)
                          .search(params)

    options = {
      links: {
        current: current_page,
        first: 1,
        last: @sale_notes.total_pages,
        prev: @sale_notes.prev_page,
        next: @sale_notes.next_page
      }
    }
    render json: SaleNoteSerializer.new(@sale_notes, options).serializable_hash, status: :ok
  end
  
  def show
    render json: SaleNoteSerializer.new(@sale_note), status: :ok
  end

  def create
    sale_note = SaleNote.new(sale_note_params)

    if sale_note.save
      render json: SaleNoteSerializer.new(sale_note).serializable_hash, status: :created
    else
      render json: { errors: sale_note.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @sale_note.update(sale_note_params)
      render json: SaleNoteSerializer.new(@sale_note).serializable_hash, status: :ok
    else
      render json: { errors: sale_note.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @sale_note.destroy
    head :no_content
  end

  private
  def set_sale_note
    @sale_note = SaleNote.find(params[:id])
  end

  def sale_note_params
    params.require(:sale_note).permit(
      :user_id,
      :client_id,
      :buy_reference_date,
      :date,
      :code,
      sale_details_attributes: [
        :id, :is_return, :container_type, :quantity, :raw_weight, :content, :sale_price, :_destroy
      ])
  end
end
