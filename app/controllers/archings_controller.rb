class ArchingsController < ApplicationController
  include Paginable

  before_action :check_login
  before_action :set_arching, only: %i[show update destroy]


  def index
    @archings = Arching.page(current_page)
                      .per(per_page)
                      .search(params)

    options = {
      links: {
        current: current_page,
        first: 1,
        last: @archings.total_pages,
        prev: @archings.prev_page,
        next: @archings.next_page
      }
    }

    render json: ArchingSerializer.new(@archings, options).serializable_hash, status: :ok
  end

  def show
    render json: ArchingSerializer.new(@arching).serializable_hash, status: :ok
  end
  
  def create
    arching = Arching.new(arching_params)

    if arching.save
      render json: ArchingSerializer.new(arching).serializable_hash, status: :created
    else
      render json: {errors: arching.errors}, status: :unprocessable_entity
    end
  end

  def update
    if @arching.update(arching_params)
      render json: ArchingSerializer.new(@arching).serializable_hash, status: :ok
    else
      render json: {errors: @arching.errors}, status: :unprocessable_entity
    end
  end

  def destroy
    @arching.destroy
    head :no_content
  end

  private 
  def set_arching
    @arching = Arching.find(params[:id])
  end

  def arching_params
    params.require(:arching).permit(:user_id, :date, :code ,:chicken_return, :weight_chicken_return, :price_per_chicken,
        arching_details_attributes: [:id, :product_type, :container_type, :racks_number, :chickens_number, :_destroy]
      )
  end
end
