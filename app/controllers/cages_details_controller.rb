class CagesDetailsController < ApplicationController
  before_action :check_login
  before_action :set_live_chicken_reception, only: %i[index]
  before_action :set_cages_detail, only: %i[update destroy]

  def index
    @cages_details = @live_chicken_reception.cages_details
    render json: CagesDetailSerializer.new(@cages_details), status: :ok
  end

  def update
    if @cages_detail.update(cages_detail_params)
      render json: @cages_detail, status: :ok
    else
      render json: @cages_detail.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @cages_detail.destroy
    head :no_content
  end

  private
  def set_live_chicken_reception
    @live_chicken_reception = LiveChickenReception.find(params[:live_chicken_reception_id])
  end

  def set_cages_detail
    @cages_detail = CagesDetail.find(params[:id])
  end

  def cages_detail_params
    params.require(:cages_detail).permit(:chickens_number, :cages_number, :cage_weight, :total_weight)
  end
end
