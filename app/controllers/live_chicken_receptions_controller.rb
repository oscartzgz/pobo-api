class LiveChickenReceptionsController < ApplicationController
  include Paginable
  before_action :check_login
  before_action :set_reception, only: %i(show update destroy)

  def index
    @chicken_receptions = LiveChickenReception.page(current_page)
                                            .per(per_page)
                                            .search(params)

    options = {
      links: {
        current: current_page,
        first: 1,
        last: @chicken_receptions.total_pages,
        prev: @chicken_receptions.prev_page,
        next: @chicken_receptions.next_page
      }
    }

    render json: LiveChickenReceptionSerializer.new(@chicken_receptions, options).serializable_hash, status: :ok
  end

  def show
    render json: LiveChickenReceptionSerializer.new(@chicken_reception).serializable_hash, status: :ok

  end

  def create
    @chicken_reception = LiveChickenReception.new(live_chicken_reception_params)
    @chicken_reception.kg_price = 0 if @chicken_reception.kg_price.blank?

    if @chicken_reception.save
      render json: LiveChickenReceptionSerializer.new(@chicken_reception).serializable_hash, status: :created
      # redirect_to live_chicken_receptions_path, notice: "Se creo la recepción de pollo vivo"
    else
      render json: { errors: @chicken_reception.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @chicken_reception.update(live_chicken_reception_params)
      render json: LiveChickenReceptionSerializer.new(@chicken_reception).serializable_hash, status: :ok
      # redirect_to live_chicken_receptions_path, notice: "Se creo la recepción de pollo vivo"
    else
      render json: { errors: @chicken_reception.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @chicken_reception.destroy
    head :no_content
  end


  private
  def live_chicken_reception_params
    params.require(:live_chicken_reception).permit(
      :user_id, :provider_id, :code, :date, :responsible_name, :details, :kg_price, :chicken_return, :weight_chicken_return,
      cages_details_attributes: [
        :id, :chickens_number, :cages_number, :cage_weight, :total_weight, :_destroy
      ]
    )
  end

  def set_reception
    @chicken_reception = LiveChickenReception.find(params[:id])
  end
end
