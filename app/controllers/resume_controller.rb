class ResumeController < ApplicationController
  before_action :check_login

  def index
    # actual_month_range = ( Date.today.beginning_of_month..Date.today.end_of_month )
    date = Date.new(2020, 12)
    actual_month_range = ( date.beginning_of_month..date.end_of_month )

    sale_notes = SaleNote.where(date: actual_month_range)

    sales_per_day = {}
    purchases_per_day = {}
    archings_per_day = {}
    lcr_total_amount_per_day = []

    actual_month_range.each{ |date| sales_per_day["#{date}"] = SaleNote.where(date: date).count }
    actual_month_range.each{ |date| purchases_per_day["#{date}"] = LiveChickenReception.where(date: date).count }
    actual_month_range.each{ |date| archings_per_day["#{date}"] = Arching.where(date: date).count }
    lcr_total_amount_per_day = actual_month_range.map{  |date| [ date.strftime('%b %-d'), LiveChickenReception.date_data(date: date)[:final_import] ] }


    data = { data: {
      total_clients: Client.count,
      total_providers: Provider.count,
      total_sales: sale_notes.count,
      sales_per_day: sales_per_day,
      purchases_per_day: purchases_per_day,
      archings_per_day: archings_per_day,
      lcr_total_amount_per_day: lcr_total_amount_per_day
    }}



    render json: data, status: :ok
  end
end
