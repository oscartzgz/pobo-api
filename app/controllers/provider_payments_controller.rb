class ProviderPaymentsController < ApplicationController

  before_action :check_login
  before_action :set_provider_payment, only: %i(show edit update destroy)
  before_action :set_provider, only: %i(index create)

  def index
    @provider_payments = @provider.provider_payments.all.order(date: :desc)
    render json: ProviderPaymentSerializer.new(@provider_payments).serializable_hash, status: :ok
  end

  def show
    render json: ProviderPaymentSerializer.new(@provider).serializable_hash, status: :ok
  end

  def create
    @provider_payment = @provider.provider_payments.build(provider_payment_params)
    
    if @provider_payment.save
      render json: ProviderPaymentSerializer.new(@provider_payment).serializable_hash, status: :created
    else
      render json: { errors: @provider_payment.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @provider_payment.update(provider_payment_params)
      render json: ProviderPaymentSerializer.new(@provider_payment).serializable_hash, status: :ok
    else
      render json: { errors: @provider_payment.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @provider_payment.destroy
    head :no_content
  end

  private
  def provider_payment_params
    params.require(:provider_payment).permit(:user_id, :provider_id, :date, :import, :details)
  end

  def set_provider_payment
    @provider_payment = ProviderPayment.find(params[:id])
  end

  def set_provider
    @provider = Provider.find(params[:provider_id])
  end

end
