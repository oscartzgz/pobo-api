module Authenticable
  def current_user
    return @current_user if @current_user

    header = request.headers['Authorization']
    return nil if header.nil?

    decoded = JsonWebToken.decode(header)

    @current_user = User.find(decoded[:user_id])


    rescue JWT::ExpiredSignature
      return nil
    rescue ActiveRecord::RecordNotFound
      return nil
  end

  protected
  def check_login
    # head :forbidden unless self.current_user
    # I think the best way to say you aren't loggedin is with unauthorized http code
    head :unauthorized unless self.current_user
  end
end