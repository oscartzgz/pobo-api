class ClientsController < ApplicationController
  before_action :check_login
  before_action :set_client, only: %i(
    show edit update destroy sale_notes
    sale_notes_with_complete_payment sale_notes_without_complete_payment)

  def index
    @clients = Client.all.order(name: :asc)
    render json: ClientSerializer.new(@clients).serializable_hash, status: :ok
  end

  def show
    render json: ClientSerializer.new(@client).serializable_hash, status: :ok
  end

  def create
    @client = Client.new(client_params)
    
    if @client.save
      render json: ClientSerializer.new(@client).serializable_hash, status: :created
    else
      render json: { errors: @client.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @client.update(client_params)
      render json: ClientSerializer.new(@client).serializable_hash, status: :ok
    else
      render json: { errors: @client.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @client.destroy
    head :no_content
  end

  def sale_notes
    sale_notes = @client.sale_notes.order(date: :asc)
    render json: SaleNoteSerializer.new(sale_notes).serializable_hash, status: :ok
  end

  def sale_notes_with_complete_payment
    sale_notes = @client.sale_notes_with_complete_payment
    render json: SaleNoteSerializer.new(sale_notes).serializable_hash, status: :ok
  end

  def sale_notes_without_complete_payment
    sale_notes = @client.sale_notes_without_complete_payment
    render json: SaleNoteSerializer.new(sale_notes).serializable_hash, status: :ok
  end

  private
  def client_params
    params.require(:client).permit(:name, :department, :municipality, :address, :email, :phone, :details)
  end

  def set_client
    @client = Client.find(params[:id] || params[:client_id])
  end
end
