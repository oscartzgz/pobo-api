class PdfsController < ApplicationControllerBase
  before_action :check_login, except: [:sale_note]
  before_action :set_sale_note, only: :sale_note

  def sale_note
        render pdf: "nota_de_venta_#{@sale_note.code}",
          template: 'pdfs/sale_note.html.erb',
          page_size: 'A6',
          page_width: '54mm',
          page_height: '20mm',
          dpi: 100,
          margin: {
            top: 5,
            right: 1,
            bottom: 5,
            left: 1
          },
          show_as_html: params.key?('debug')

        # pdf_html = ActionController::Base.new.render_to_string(template: 'pdfs/sale_note.html.erb')
        # pdf = WickedPdf.new.pdf_from_string(pdf_html)
        # send_data pdf, filename: 'file_name.pdf'
  end

  private
  def set_sale_note
    @sale_note = SaleNote.find(params[:id])
  end
end
