class ExpensesController < ApplicationController
  include Paginable

  before_action :check_login
  before_action :set_expense, only: %i(show edit update destroy)

  def index
    @expenses = Expense.page(current_page)
                          .per(per_page)
                          .search(params)

    options = {
      links: {
        current: current_page,
        first: 1,
        last: @expenses.total_pages,
        prev: @expenses.prev_page,
        next: @expenses.next_page
      }
    }
    render json: ExpenseSerializer.new(@expenses, options).serializable_hash, status: :ok
  end

  def show
    render json: ExpenseSerializer.new(@expense).serializable_hash, status: :ok
  end

  def create
    @expense = Expense.new(expense_params)
    
    if @expense.save
      render json: ExpenseSerializer.new(@expense).serializable_hash, status: :created
    else
      render json: { errors: @expense.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @expense.update(expense_params)
      render json: ExpenseSerializer.new(@expense).serializable_hash, status: :ok
    else
      render json: { errors: @expense.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @expense.destroy
    head :no_content
  end

  private
  def expense_params
    params.require(:expense).permit(:kind, :date, :import, :details)
  end

  def set_expense
    @expense = Expense.find(params[:id])
  end
end
