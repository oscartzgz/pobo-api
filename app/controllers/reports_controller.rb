class ReportsController < ApplicationController
  include ActionView::Rendering

  before_action :check_login
  before_action :set_range, only: %i[reception_and_arching provider_resume collections]
  before_action :set_provider, only: %i[provider_resume collections]

  def collections
    clients = Client.find(params[:client_id]) if params[:client_id].present? 
    
    sale_notes = SaleNote.includes(:sale_details).where(date: @range)

    sale_notes = sale_notes.where(client: clients) if clients.present?

    clients_ids = sale_notes.distinct.pluck(:client_id)
    clients = Client.find(clients_ids)

    @report_data = {data: []}

    clients.each do |client|
      sales = sale_notes.where(client:  client)
      @report_data[:data] << {
        client: client, 
        sales_data: sales.map { |sale| {sale_note: SaleNoteSerializer.new(sale).serializable_hash, sale_details: SaleDetailSerializer.new(sale.sale_details).serializable_hash} }
      }
    end


    respond_to do |format|
      format.xlsx {
        filename = "recepcion_pollo_vivo-#{@range.first.strftime('%d-%m-%Y')}_al_#{@range.end.strftime('%d-%m-%Y')}.xlsx"
        render xlsx: 'collections', template: 'reports/collections', filename: filename
      }
      format.json { render json: @report_data }
    end
  end


  def reception_and_arching
    receptions = LiveChickenReception.where(date: @range)

    @report_data = receptions.map do |item|
      {
        date: item.date.strftime('%d/%m/%Y'),
        code: item.code.to_s.rjust(6, '0'),
        total_chickens: item.total_chickens,
        raw_weight: item.raw_weight,
        cages_weight: item.cages_weight,
        chicken_weight: item.chicken_weight,
        net_weight: item.net_weight,
        final_net_weight: item.final_net_weight,
        weight_chicken_return: item.weight_chicken_return,
        kg_price: item.kg_price,
        slaughter_price: item.slaughter_price,
        import: item.import,
        final_import: item.final_import,
      }
    end

    respond_to do |format|
      format.xlsx {
        filename = "recepcion_pollo_vivo-#{@range.first.strftime('%d-%m-%Y')}_al_#{@range.end.strftime('%d-%m-%Y')}.xlsx"
        render xlsx: 'reception_and_arching', template: 'reports/reception_and_arching', filename: filename
      }
      format.json { render json: @report_data }
    end
  end

  def provider_resume
    receptions = LiveChickenReception.where(date: @range, provider: @provider).order(date: :asc)

    provider_data = ProviderSerializer.new(@provider).serializable_hash

    report_items = receptions.map do |item|
     {
        id:  item.id,
        date: item.date.strftime('%d/%m/%Y'),
        code: item.code.to_s.rjust(6, '0'),
        total_chickens: item.total_chickens,
        raw_weight: item.raw_weight,
        cages_weight: item.cages_weight,
        chicken_weight: item.chicken_weight,
        decrease: item.decrease,
        net_weight: item.net_weight,
        final_net_weight: item.final_net_weight,
        weight_chicken_return: item.weight_chicken_return,
        kg_price: item.kg_price,
        slaughter_price: item.slaughter_price,
        slaughter_import: item.slaughter_import.truncate(2),
        import: item.import.truncate(2),
        final_import: item.final_import.truncate(2),
      }
    end

    resume = {
    }

    @report_data = {
      data: {
        provider: provider_data[:data],
        live_chicken_receptions: report_items,
        total_final_import: report_items.map{|i| i[:final_import]}.sum
      } 
    }

    respond_to do |format|
      format.xlsx {
        provider_name = @report_data[:data][:provider][:attributes][:name].downcase.split(' ').join('_')
        filename = "#{provider_name}-#{@range.first.strftime('%d-%m-%Y')}_al_#{@range.end.strftime('%d-%m-%Y')}.xlsx"
        render xlsx: 'provider_resume', template: 'reports/provider_resume', filename: filename
      }
      format.json { render json: @report_data }
    end
  end

  
  private

  def set_range
    start_date, end_date = params[:start].to_date, params[:end].to_date
    @range = (start_date..end_date)
  end

  def set_provider
    @provider = Provider.find(params[:provider_id]) if params[:provider_id]
    @provider = Provider.all unless params[:provider_id]
  end

  def render_to_body(options)
    _render_to_body_with_renderer(options) || super
  end
end
