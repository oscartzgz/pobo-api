class CalendarController < ApplicationController
  before_action :check_login
  before_action :search_month_data, only: %i[index]
  before_action :set_date_data, only: %i[date]

  def index
    # date = params[:date]
    # @receptions = LiveChickenReception.this_month
    # @archings   = Arching.this_month
    # @sale_notes = SaleNote.this_month

    # render json: {
    #   live_chicken_receptions: LiveChickenReceptionSerializer.new(@receptions).serializable_hash,
    #   archings: ArchingSerializer.new(@archings).serializable_hash,
    #   sale_notes: SaleNoteSerializer.new(@sale_notes).serializable_hash
    # }, status: :ok

    render json: {data: @day_by_day}

  end

  def date
    render json: {data: @date_data}
  end

  private
  def search_month_data
    if params[:this_month].present? && params[:this_month] == 'true'
      this_month_range = (Date.today.beginning_of_month..Date.today.end_of_month)
      set_month_data(this_month_range)
    elsif params[:year].present? && params[:month].present?
      year, month = params[:year].to_i, params[:month].to_i
      date = Date.new(year, month)
      month_range = (date.beginning_of_month..date.end_of_month)
      set_month_data(month_range)
    else
      return nil
    end
  end

  def set_date_data
    return unless params[:date].present?
    date = params[:date].to_date

    receptions = LiveChickenReception.where(date: date)
    archings   = Arching.where(date: date)
    sale_notes = SaleNote.where(buy_reference_date: date)
    expenses   = Expense.where(date: date)

    expenses_amount = 0
    earning_amount = 0

    receptions.each do |reception|
      # expenses_amount += reception.final_import
      expenses_amount += reception.import
    end
    expenses.each do |expense|
      expenses_amount += expense.import
    end

    @date_data = {
      date: Time.zone.parse( date.to_s  ).iso8601,
      live_chicken_receptions: LiveChickenReceptionSerializer.new(receptions).serializable_hash[:data],
      archings: ArchingSerializer.new(archings).serializable_hash[:data],
      sale_notes: SaleNoteSerializer.new(sale_notes).serializable_hash[:data],
      expenses: ExpenseSerializer.new(expenses).serializable_hash[:data],
      resume: {
        expenses_amount: expenses_amount,
        earning_amount: sale_notes.all.map { |sale| sale.total_amount }.sum
      }
    }
    @date_data
  end

  def set_month_data(month_range)
    @receptions = LiveChickenReception.where(date: month_range)
    @archings   = Arching.where(date: month_range)
    @sale_notes = SaleNote.where(buy_reference_date: month_range)


    @day_by_day = month_range.map do |date|
      {
        date: Time.zone.parse( date.to_s  ).iso8601,
        live_chicken_receptions: @receptions.where(date: date),
        archings: @archings.where(date: date),
        sale_notes: @sale_notes.where(buy_reference_date: date)
      }
    end
  end
end