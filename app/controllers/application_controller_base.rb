class ApplicationControllerBase < ActionController::Base
  include Authenticable
  include ActionController::MimeResponds
end
