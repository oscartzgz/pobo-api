class ProvidersController < ApplicationController
  before_action :check_login
  before_action :set_provider, only: %i(show edit update destroy)

  def index
    @providers = Provider.all.order(name: :asc)
    render json: ProviderSerializer.new(@providers).serializable_hash, status: :ok
  end

  def show
    render json: ProviderSerializer.new(@provider).serializable_hash, status: :ok
  end

  def create
    @provider = Provider.new(provider_params)
    
    if @provider.save
      render json: ProviderSerializer.new(@provider).serializable_hash, status: :created
    else
      render json: { errors: @provider.errors }, status: :unprocessable_entity
    end
  end

  def update
    if @provider.update(provider_params)
      render json: ProviderSerializer.new(@provider).serializable_hash, status: :ok
    else
      render json: { errors: @provider.errors }, status: :unprocessable_entity
    end
  end

  def destroy
    @provider.destroy
    head :no_content
  end

  private
  def provider_params
    params.require(:provider).permit(:name, :department, :municipality, :address, :email, :phone, :details, :rake_weight, :our_debt_date, :our_debt)
  end

  def set_provider
    @provider = Provider.find(params[:id])
  end
end
