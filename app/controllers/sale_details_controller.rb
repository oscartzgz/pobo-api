class SaleDetailsController < ApplicationController
  before_action :check_login
  before_action :set_sale_note, only: %i[index]

  def index
    @sale_details = @sale_note.sale_details
    render json: SaleDetailSerializer.new(@sale_details), status: :ok
  end

  private
  def set_sale_note
    @sale_note = SaleNote.find(params[:sale_note_id])
  end

end
