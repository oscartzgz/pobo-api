class ArchingDetailsController < ApplicationController
  before_action :check_login
  before_action :set_arching, only: %i[index]

  @@DISCOUNT_CONTAINER = 'Rejilla'

  def self.container_types
    [@@DISCOUNT_CONTAINER, 'Llevo', 'Matadero']
  end

  def index
    @arching_details = @arching.arching_details
    render json: ArchingDetailSerializer.new(@arching_details).serializable_hash, status: :ok
  end

  private
  def set_arching
    @arching = Arching.find(params[:arching_id])
  end
end
