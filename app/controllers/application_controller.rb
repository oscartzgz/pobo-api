class ApplicationController < ActionController::API
  include Authenticable
  include ActionController::MimeResponds
end
