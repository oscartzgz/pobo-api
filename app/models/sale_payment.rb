class SalePayment < ApplicationRecord
  belongs_to :client
  has_many :sale_payment_details, dependent: :destroy
  has_many :sale_notes, through: :sale_payment_details

  accepts_nested_attributes_for :sale_payment_details, allow_destroy: true

  before_save :format_code_with_zeros
  
  def sale_notes_amount
    self.sale_notes.sum(&:total_amount)
  end

  def difference
    sale_notes_amount - self.import
  end

  private
  def format_code_with_zeros
    self.code = self.code.rjust(6, "0") if self.code.scan(/\D/).empty?
  end
end
