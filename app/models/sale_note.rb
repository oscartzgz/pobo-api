class SaleNote < ApplicationRecord
  validates :user_id, presence: true
  validates :date, presence: true
  validates :client_id, presence: true
  validates :buy_reference_date, presence: true
  
  belongs_to  :user
  belongs_to  :client
  has_many    :sale_details, dependent: :destroy
  has_many    :sale_payment_details, dependent: :destroy

  before_save :format_code_with_zeros

  accepts_nested_attributes_for :sale_details, allow_destroy: true
  
  scope :this_month, -> { where(date: Time.now.beginning_of_month..Time.now.end_of_month) }

  def self.search(params = {})
    sale_notes = params[:sale_note_ids].present? ? SaleNote.where(id: JSON.parse(params[:sale_note_ids])) : SaleNote.all.order(date: :desc)
    sale_notes = sale_notes.order(date: params[:order]) if params[:order] && [:asc, :desc].include?(params[:order])
    sale_notes = sale_notes.where(date: params[:date].to_date) if params[:date].present?
    sale_notes = sale_notes.where(buy_reference_date: params[:buy_reference_date].to_date) if params[:buy_reference_date].present?
    sale_notes = sale_notes.where(date: (params[:start].to_date..params[:end].to_date) ) if params[:start].present? && params[:end].present?
    sale_notes = sale_notes.where(client_id: params[:client_id]) if params[:client_id].present?
    sale_notes = sale_notes.where(user_id: params[:user_id]) if params[:user_id].present?
    sale_notes = sale_notes.filter_by_code(params[:code]) if params[:code].present?
    sale_notes = sale_notes.limit(params[:limit]) if params[:limit].present?
    sale_notes
  end

  scope :filter_by_code, lambda { |keyword|
    where('lower(code) LIKE ?', "%#{keyword.downcase}%")
  }

  def details # Not Returned Details
    sale_details.where(is_return: false)
  end

  def returned_details
    sale_details.where(is_return: true)
  end

  def raw_weight
    details.map{|detail| (detail.raw_weight || 0)}.sum
  end

  def chicken_raw_weight
    details.select{ |detail| SaleDetail.PRIMARY_PRODUCT.include? detail.content }.sum(&:raw_weight)
  end

  def chicken_weight
    details.where(content: SaleDetail.PRIMARY_PRODUCT, container_type: SaleDetail.DISCOUNT_CONTAINER).map{|detail| (detail.raw_weight || 0)}.sum
  end

  def returned_chicken_weight
    returned_details.where(content: SaleDetail.PRIMARY_PRODUCT, container_type: SaleDetail.DISCOUNT_CONTAINER).map{|detail| (detail.raw_weight || 0)}.sum
  end

  def untare
    untare = details.where( container_type: SaleDetail.DISCOUNT_CONTAINER )
    untare.map{ |d| d.quantity * SaleDetail.RACKE_WEIGHT }.sum
  end

  def chicken_untare
    untare = details.where( content: SaleDetail.PRIMARY_PRODUCT, container_type: SaleDetail.DISCOUNT_CONTAINER ).map do |detail|
      detail.quantity * SaleDetail.RACKE_WEIGHT
    end
    untare.compact.sum
  end

  def chicken_net_weight
    chicken_raw_weight - chicken_untare
  end

  def chicken_amount
    sub_totals = details.where( content: SaleDetail.PRIMARY_PRODUCT ).map do |detail|
      detail.final_amount
    end
    sub_totals.compact.sum
  end
  
  def net_weight
    raw_weight - untare
  end

  def chicken_price
    details.map{ |d| d.sale_price }
  end

  def chicken_amount
    details.where( content: SaleDetail.PRIMARY_PRODUCT, container_type: SaleDetail.DISCOUNT_CONTAINER ).map{ |detail| detail.final_amount }.sum
  end

  def returned_chicken_amount
    returned_details.where( content: SaleDetail.PRIMARY_PRODUCT, container_type: SaleDetail.DISCOUNT_CONTAINER ).map{ |detail| detail.final_amount }.sum
  end

  def total_amount
    details.map{ |detail| detail.final_amount }.sum - returned_details.map{ |detail| detail.final_amount }.sum
  end

  private
  def format_code_with_zeros
    self.code = self.code.rjust(6, "0") if self.code.scan(/\D/).empty?
  end
end
