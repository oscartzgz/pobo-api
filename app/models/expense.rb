class Expense < ApplicationRecord
  validates :kind, presence: true
  validates :date, presence: true
  validates :import, presence: true

  def self.search(params = {})
    expenses = params[:expense_ids].present? ? Expense.where(id: params[:expense_ids]) : Expense.all.order(date: :desc)
    expenses = expenses.where(date: params[:date].to_date) if params[:date].present?
    expenses = expenses.where(date: (params[:start].to_date..params[:end].to_date) ) if params[:start].present? && params[:end].present?
    expenses = expenses.where(kind: params[:kind]) if params[:kind].present?
    expenses
  end
end
