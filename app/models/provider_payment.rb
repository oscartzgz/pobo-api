class ProviderPayment < ApplicationRecord
  validates :user_id, presence: true
  validates :provider_id, presence: true
  validates :date, presence: true
  validates :import, presence: true

  belongs_to :user
  belongs_to :provider
end
