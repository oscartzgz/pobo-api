class SaleDetail < ApplicationRecord
  belongs_to :sale_note

  @@DISCOUNT_CONTAINER = 'Rejilla'
  @@PRIMARY_PRODUCT = ['Pollo', 'Menudo (Como pollo)']
  # @@AS_PRIMARY_PRODUCT = 
  @@RACKE_WEIGHT = 2

  before_save :set_min_quantity

  def self.container_types
    [@@DISCOUNT_CONTAINER, 'Bolsa']
  end

  def self.content_types
    @@PRIMARY_PRODUCT + ['Menudo', 'Alas', 'Higado', 'Patas']
  end

  def rack_untare
    return quantity * @@RACKE_WEIGHT if container_type == @@DISCOUNT_CONTAINER
    return 0
  end

  def chicken_raw_weight
    return (quantity * raw_weight) if @@PRIMARY_PRODUCT.include?(content)
    return 0
  end

  def chicken_rack_untare
    return quantity * @@RACKE_WEIGHT if (container_type == @@DISCOUNT_CONTAINER and @@PRIMARY_PRODUCT.include?(content))
    return 0
  end

  def chicken_net_weight
    return chicken_raw_weight - chicken_rack_untare
    return 0
  end

  # ???????????
  def net_weight
    raw_weight - rack_untare
  end

  def import
    net_weight * sale_price
  end

  def final_amount # Alias of import method
    import
  end

  def self.DISCOUNT_CONTAINER; @@DISCOUNT_CONTAINER;  end
  def self.PRIMARY_PRODUCT; @@PRIMARY_PRODUCT;  end
  def self.RACKE_WEIGHT; @@RACKE_WEIGHT;  end

  private 
  def set_min_quantity
    self.quantity = 1 if (self.quantity < 1)
  end
end
