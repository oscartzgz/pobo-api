class ArchingDetail < ApplicationRecord
  belongs_to  :arching

  def total_chickens
    (racks_number.to_d == 0 ? 1 : racks_number) * chickens_number
  end
end
