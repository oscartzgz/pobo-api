class User < ApplicationRecord
  validates :email, uniqueness: true
  validates_format_of :email, with: /@/
  validates :password_digest, presence: true

  has_secure_password

  has_many :live_chicken_receptions, dependent: :destroy
  has_many :archings, dependent: :destroy
  has_many :sale_notes, dependent: :destroy
end
