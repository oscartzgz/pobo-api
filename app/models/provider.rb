class Provider < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :rake_weight, presence: true
  validates :our_debt_date, presence: true
  validates :our_debt, presence: true

  has_many :live_chicken_receptions
  has_many :provider_payments

  def self.departments_list
    [
      'Beni',
      'Chuquisaca',
      'Cochabamba',
      'La Paz',
      'Oruro',
      'Pando',
      'Potosí',
      'Santa Cruz',
      'Tarija',
    ] 
  end


  def our_doubt_by_date(date)
    receptions = live_chicken_receptions.where('date < ? ',  (date + 1.day))
    doubt_of_purchases = receptions.sum{ |lcr| lcr.final_import }

    doubt_of_purchases + our_debt
  end

  def actual_doubt
    doubt_of_purchases = live_chicken_receptions.sum{ |lcr| lcr.final_import  }
    # our_debt_date

    actual_doubt = doubt_of_purchases + our_debt

    actual_doubt - provider_payments.sum(:import)
  end
end
