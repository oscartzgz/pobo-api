class SalePaymentDetail < ApplicationRecord
  belongs_to :sale_note
  belongs_to :sale_payment

  scope :with_complete_payment, -> { where(complete_payment: true) }
  # Descartar los completos
  scope :without_complete_payment, -> { where(complete_payment: false) }
end
