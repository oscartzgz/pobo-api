class Arching < ApplicationRecord
  validates :user_id, presence: true
  validates :date, presence: true
  validates :code, presence: true
  validates :price_per_chicken, presence: true
  validates :weight_chicken_return, presence: true, if: :chicken_return_present?

  belongs_to :user
  has_many :arching_details, dependent: :destroy

  accepts_nested_attributes_for :arching_details, allow_destroy: true

  before_save :format_code_with_zeros


  def self.search(params = {})
    archings = params[:archings_ids].present? ? Arching.where(id: params[:archings_ids]) : Arching.all.order(date: :desc)
    archings = archings.where(date: params[:date].to_date) if params[:date].present?
    archings = archings.where(date: (params[:start].to_date..params[:end].to_date) ) if params[:start].present? && params[:end].present?
    archings = archings.where(user_id: params[:user_id]) if params[:user_id].present?
    archings = archings.filter_by_code(params[:code]) if params[:code].present?
    archings = archings.limit_number(params[:limit]) if params[:limit].present?
    archings
  end

  scope :limit_number, lambda { |number|
    limit(number)
  }

  scope :filter_by_code, lambda { |keyword|
    where('lower(code) LIKE ?', "%#{keyword.downcase}%")
  }

  scope :this_month, -> { where(date: Time.now.beginning_of_month..Time.now.end_of_month)}
  

  def total_chickens # Pollo total + DEVOLUCION
    total = 0
    arching_details.map{ |detail| detail.total_chickens }.sum + (chicken_return || 0 )
  end

  def chicken_return_present?
    chicken_return.present?
  end

  private
  def format_code_with_zeros
    self.code = self.code.rjust(6, "0") if self.code.scan(/\D/).empty?
  end
end
