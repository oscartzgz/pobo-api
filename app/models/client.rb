class Client < ApplicationRecord
  validates :name, presence: true
  validates :name, uniqueness: true
  validates :department, presence: true

  has_many :sale_notes
  has_many :sale_payments
  has_many :sale_payment_details, through: :sale_payments

  def self.departments_list
    [
      'Beni',
      'Chuquisaca',
      'Cochabamba',
      'La Paz',
      'Oruro',
      'Pando',
      'Potosí',
      'Santa Cruz',
      'Tarija',
    ] 
  end

  def sale_notes_with_complete_payment
    sale_notes.where( id: sale_payment_details.with_complete_payment.pluck(:sale_note_id).uniq )
  end

  def sale_notes_without_complete_payment
    sale_notes.where.not(
      id: sale_payment_details.with_complete_payment.pluck(:sale_note_id).uniq )
  end
end
