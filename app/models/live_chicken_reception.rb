class LiveChickenReception < ApplicationRecord
  MERMA = 0.16

  validates :user, presence: true
  validates :provider, presence: true
  validates :date, presence: true
  validates :code, presence: true

  belongs_to  :user
  belongs_to  :provider
  has_many    :cages_details, dependent: :destroy
  has_one     :arching
  # has_many    :sale_notes
  # has_many    :sale_details, through: :sale_note

  before_save :format_code_with_zeros

  accepts_nested_attributes_for :cages_details, allow_destroy: true

  def self.search(params = {})
    receptions = params[:live_chicken_reception_ids].present? ? LiveChickenReception.where(id: params[:live_chicken_reception_ids]) : LiveChickenReception.all.order(date: :desc)
    receptions = receptions.where(date: params[:date].to_date) if params[:date].present?
    receptions = receptions.where(date: (params[:start].to_date..params[:end].to_date) ) if params[:start].present? && params[:end].present?
    receptions = receptions.where(provider_id: params[:provider_id]) if params[:provider_id].present?
    receptions = receptions.where(user_id: params[:user_id]) if params[:user_id].present?
    receptions = receptions.filter_by_code(params[:code]) if params[:code].present?
    receptions = receptions.limit(params[:limit]) if params[:limit].present?
    receptions
  end

  scope :limit_number, lambda { |number|
    limit(number)
  }

  scope :filter_by_code, lambda { |keyword|
    where('lower(code) LIKE ?', "%#{keyword.downcase}%")
  }

  scope :this_month, -> { where(date: Time.now.beginning_of_month..Time.now.end_of_month)}

  def self.date_data(params = {date: '', range: {start: '', end: ''}})
    date = params.dig( :date )
    range_start = params.dig( :range, :start )
    range_end = params.dig( :range, :end )

    if range_start.present? and range_end.present?
      range = (range_start..range_end)
      live_chicken_receptions = self.where(date: range)
      total_import = live_chicken_receptions.map{ |i| i.final_import }.sum
      data = { final_import: total_import}

    elsif date.present?
      live_chicken_receptions = self.where(date: date)
      total_import = live_chicken_receptions.map{ |i| i.final_import }.sum
      data = { final_import: total_import}
    end

    data
  end


  def slaughter_price
    arching = Arching.where(date: date).take
    slaughter_price = arching.present? ? arching.price_per_chicken : 0
    slaughter_price
  end

  def slaughter_import
    arching = Arching.where(date: date).take
    slaughter_price = arching.present? ? arching.price_per_chicken : 0
    total_chickens * slaughter_price
  end

  def cages_weight # Peso de las Jaulas
    cages_details.map { |item| (item.cages_number || 0) * (item.cage_weight || 0) }.compact.sum
  end 

  def chicken_weight # Peso de solo Aves
    total_cages_weights = cages_weight
    raw_weight - total_cages_weights
  end

  def weight_avg
    # final_net_weight / total_chickens
    net_weight / total_chickens
  end

  def total_chickens # Destare total
    cages_details.sum(:chickens_number)
  end

  def raw_weight # Peso bruto, Pollo y Jaulas
    cages_details.sum(:total_weight)
  end

  def decrease # Merma
    chicken_weight * MERMA
  end

  def net_weight # Peso Neto = Peso Neto - Merma
    chicken_weight - decrease
  end

  def final_net_weight
    (net_weight || 0) - (weight_chicken_return || 0 )
  end

  def import 
    return '' if !total_chickens.present? || !kg_price.present?
    final_net_weight * kg_price
  end

  def final_import
    import - slaughter_import
  end

  def has_arching?
    !self.arching.nil?
  end

  
  private 

  def format_code_with_zeros
    self.code = self.code.rjust(6, "0") if self.code.scan(/\D/).empty?
  end
end
